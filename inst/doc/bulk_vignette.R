## ----"installation", eval=FALSE-----------------------------------------------
#  if (!requireNamespace("BiocManager", quietly = TRUE))
#      install.packages("BiocManager")
#  
#  BiocManager::install("dorothea")

## ----"load packages", message = FALSE-----------------------------------------
## We load the required packages
library(dorothea)
library(bcellViper)
library(dplyr)
library(viper)

## ----"load data"--------------------------------------------------------------
# accessing expression data from bcellViper
data(bcellViper, package = "bcellViper")

# acessing (human) dorothea regulons
# for mouse regulons: data(dorothea_mm, package = "dorothea")
data(dorothea_hs, package = "dorothea")

## ----"run viper", message=FALSE-----------------------------------------------
regulons = dorothea_hs %>%
  filter(confidence %in% c("A", "B"))

tf_activities <- run_viper(dset, regulons, 
                           options =  list(method = "scale", minsize = 4, 
                                           eset.filter = FALSE, cores = 1, 
                                           verbose = FALSE))

## ----sessionInfo, echo=FALSE--------------------------------------------------
sessionInfo()

